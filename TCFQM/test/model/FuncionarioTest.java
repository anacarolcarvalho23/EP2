package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class FuncionarioTest {
    
    public FuncionarioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetSenha() {
        System.out.println("getSenha");
        Funcionario instance = new Funcionario();
        String expResult = "senha teste";
        instance.setSenha("senha teste");
        String result = instance.getSenha();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetSenha() {
        System.out.println("setSenha");
        String senha = "";
        Funcionario instance = new Funcionario();
        instance.setSenha(senha);
    }

    @Test
    public void testGetLogin() {
        System.out.println("getLogin");
        Funcionario instance = new Funcionario();
        String expResult = "teste login";
        instance.setLogin("teste login");
        String result = instance.getLogin();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetLogin() {
        System.out.println("setLogin");
        String login = "";
        Funcionario instance = new Funcionario();
        instance.setLogin(login);
    }
    
}
