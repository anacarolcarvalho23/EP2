package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ComidaTest {
    
    public ComidaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Comida instance = new Comida();
        String expResult = "teste";
        instance.setNome("teste");
        String result = instance.getNome();
        assertEquals(expResult, result);

    }

    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String nome = "";
        Comida instance = new Comida();
        instance.setNome(nome);
    }

    @Test
    public void testGetPreco() {
        System.out.println("getPreco");
        Comida instance = new Comida();
        int expResult = 10;
        instance.setPreco(10);
        int result = instance.getPreco();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetPreco() {
        System.out.println("setPreco");
        int preco = 0;
        Comida instance = new Comida();
        instance.setPreco(preco);
    }
    
}
