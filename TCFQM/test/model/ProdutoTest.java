package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProdutoTest {
    
    public ProdutoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Produto instance = new Produto();
        String expResult = "teste";
        instance.setNome("teste");
        String result = instance.getNome();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetNome() {
        System.out.println("setNome");
        String nome = "";
        Produto instance = new Produto();
        instance.setNome(nome);
    }

    @Test
    public void testGetPreco() {
        System.out.println("getPreco");
        Produto instance = new Produto();
        double expResult = 10.0;
        instance.setPreco(10.0);
        double result = instance.getPreco();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetPreco() {
        System.out.println("setPreco");
        double preco = 0.0;
        Produto instance = new Produto();
        instance.setPreco(preco);
    }
    
}
