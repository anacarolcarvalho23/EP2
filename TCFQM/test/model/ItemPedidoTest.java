package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ItemPedidoTest {
    
    public ItemPedidoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetQuantidade() {
        System.out.println("getQuantidade");
        ItemPedido instance = null;
        int expResult = 10;
        instance.setQuantidade(10);
        int result = instance.getQuantidade();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetQuantidade() {
        System.out.println("setQuantidade");
        int quantidade = 0;
        ItemPedido instance = null;
        boolean expResult = false;
        boolean result = instance.setQuantidade(quantidade);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetValor() {
        System.out.println("getValor");
        ItemPedido instance = null;
        double expResult = 10.0;
        instance.setValor(10.0);
        double result = instance.getValor();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetValor() {
        System.out.println("setValor");
        double valor = 0.0;
        ItemPedido instance = null;
        instance.setValor(valor);
    }

    @Test
    public void testGetObservacao() {
        System.out.println("getObservacao");
        ItemPedido instance = null;
        String expResult = "teste observação";
        instance.setObservacao("teste observação");
        String result = instance.getObservacao();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetObservacao() {
        System.out.println("setObservacao");
        String observacao = "";
        ItemPedido instance = null;
        instance.setObservacao(observacao);
    }
    
}
