package model;

public class Pedido {
    	private
	double valor;
	ItemPedido item;
	Pagamento pagamento;
        
        public Pedido(ItemPedido item, Pagamento pagamento){
            this.item = item;
            this.pagamento = pagamento;
        }
	
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public ItemPedido getItem() {
		return item;
	}
	public void setItem(ItemPedido item) {
		this.item = item;
	}
	public Pagamento getPagamento() {
		return pagamento;
	}
	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}
}
