package model;

public class ItemPedido {
	private	int quantidade;
	private double valor;
	private String observacao;
	private Comida comida;
        private Bebida bebida;
        private Sobremesa sobremesa;
        
	public ItemPedido(Comida comida, Bebida bebida, Sobremesa sobremesa){
            this.comida = comida;
            this.bebida = bebida;
            this.sobremesa = sobremesa;
}
        
	public int getQuantidade() {
		return quantidade;
	}
	public boolean setQuantidade(int quantidade) {
		this.quantidade = quantidade;
		return true;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

}
