package model;

public class Pagamento {
    
    private boolean pagamento;
    private String tipoPagamento;
    
    public Pagamento(){
        
        
    }
    
    	public boolean getPagamento() {
		return pagamento;
	}
	public void setPagamento(boolean pagamento) {
		this.pagamento = pagamento;
	}
    
        public String getTipoPagamento() {
		return tipoPagamento;
	}
	public void setTipoPagamento(String tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}
}
