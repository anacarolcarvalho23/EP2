package view;

import controller.ManipularArquivo;
import static java.awt.SystemColor.menu;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Bebida;
import model.Comida;
import model.Sobremesa;

public class TelaMenu extends javax.swing.JFrame {

    
    
    public TelaMenu() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelRestaurante = new javax.swing.JLabel();
        bPedido = new javax.swing.JButton();
        bCliente = new javax.swing.JButton();
        bEstoque = new javax.swing.JButton();
        bVoltar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelRestaurante.setText("Tô Com Fome Quero Mais");

        bPedido.setText("Pedido");
        bPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bPedidoActionPerformed(evt);
            }
        });

        bCliente.setText("Cliente");
        bCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bClienteActionPerformed(evt);
            }
        });

        bEstoque.setText("Estoque");
        bEstoque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEstoqueActionPerformed(evt);
            }
        });

        bVoltar.setText("Voltar");
        bVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bVoltarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(bVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(153, 153, 153)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(bPedido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(bEstoque, javax.swing.GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addComponent(labelRestaurante)))
                        .addGap(0, 102, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelRestaurante)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(bPedido)
                .addGap(27, 27, 27)
                .addComponent(bCliente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addComponent(bEstoque)
                .addGap(18, 18, 18)
                .addComponent(bVoltar)
                .addGap(41, 41, 41))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bVoltarActionPerformed
        TelaLogin telaLogin = new TelaLogin();
        telaLogin.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_bVoltarActionPerformed

    private void bPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bPedidoActionPerformed
        TelaPedido telaPedido = new TelaPedido();
        telaPedido.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_bPedidoActionPerformed

    private void bClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bClienteActionPerformed
        TelaCadCliente telaCadCliente = new TelaCadCliente();
        telaCadCliente.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_bClienteActionPerformed

    private void bEstoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEstoqueActionPerformed
        TelaEstoque telaEstoque = null;
        try {
            telaEstoque = new TelaEstoque();
        } catch (IOException ex) {
            Logger.getLogger(TelaMenu.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //VC TEM QUE FAZER ESSA LINHA ABAIXO PRA NAO MOSTRAR A TELA DEPOIS QUE VC SAIR
        this.setVisible(false);
        telaEstoque.setVisible(true);
    }//GEN-LAST:event_bEstoqueActionPerformed

    public static void main(String args[]){
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
                
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bCliente;
    private javax.swing.JButton bEstoque;
    private javax.swing.JButton bPedido;
    private javax.swing.JButton bVoltar;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel labelRestaurante;
    // End of variables declaration//GEN-END:variables
}
