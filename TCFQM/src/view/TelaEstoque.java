package view;

import controller.EstoqueController;
import controller.ManipularArquivo;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.GroupLayout.Alignment.CENTER;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import model.Sobremesa;

public class TelaEstoque extends javax.swing.JFrame {

    
    
    public TelaEstoque() throws IOException {
        initComponents();
        }
        
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelRestauranteEst = new javax.swing.JLabel();
        labelProdutoEst = new javax.swing.JLabel();
        textProdutoEstoque = new javax.swing.JTextField();
        bAdicionarEst = new javax.swing.JButton();
        bVoltarEst = new javax.swing.JButton();
        labelCategoriaEst = new javax.swing.JLabel();
        listCategoriaEst = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList3 = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        textPrecoEstoque = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelRestauranteEst.setText("Tô Com Fome Quero Mais");

        labelProdutoEst.setText("Produto");

        bAdicionarEst.setText("Adicionar");
        bAdicionarEst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAdicionarEstActionPerformed(evt);
            }
        });

        bVoltarEst.setText("VOLTAR");
        bVoltarEst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bVoltarEstActionPerformed(evt);
            }
        });

        labelCategoriaEst.setText("Categoria");

        listCategoriaEst.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Comida", "Bebida", "Sobremesa" }));

        try {
            jList1.setModel(new javax.swing.AbstractListModel<String>() {
                String[] strings = ManipularArquivo.pegaDoArquivo("Sobremesa");
                public int getSize() { return strings.length; }
                public String getElementAt(int i) { return strings[i]; }
            });
        } catch (IOException ex) {
            Logger.getLogger(TelaEstoque.class.getName()).log(Level.SEVERE, null, ex);
        }
        jScrollPane1.setViewportView(jList1);

        try {
            jList2.setModel(new javax.swing.AbstractListModel<String>() {
                String[] strings = ManipularArquivo.pegaDoArquivo("Bebida");
                public int getSize() { return strings.length; }
                public String getElementAt(int i) { return strings[i]; }
            });
        } catch (IOException ex) {
            Logger.getLogger(TelaEstoque.class.getName()).log(Level.SEVERE, null, ex);
        }
        jScrollPane2.setViewportView(jList2);

        try {
            jList3.setModel(new javax.swing.AbstractListModel<String>() {
                String[] strings = ManipularArquivo.pegaDoArquivo("Comida");
                public int getSize() { return strings.length; }
                public String getElementAt(int i) { return strings[i]; }
            });
        } catch (IOException ex) {
            Logger.getLogger(TelaEstoque.class.getName()).log(Level.SEVERE, null, ex);
        }
        jScrollPane3.setViewportView(jList3);

        jLabel1.setText("Sobremesa");

        jLabel2.setText("Bebida");

        jLabel3.setText("Comida");

        jLabel4.setText("Preço");

        textPrecoEstoque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textPrecoEstoqueActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(110, 110, 110)
                        .addComponent(labelRestauranteEst)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelCategoriaEst)
                            .addComponent(labelProdutoEst))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(listCategoriaEst, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(textProdutoEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textPrecoEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(bAdicionarEst)))))
                .addGap(26, 26, 26))
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(135, 135, 135)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bVoltarEst, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 130, Short.MAX_VALUE)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33))))
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addGap(182, 182, 182)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(55, 55, 55))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelRestauranteEst)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textProdutoEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bAdicionarEst)
                    .addComponent(labelProdutoEst)
                    .addComponent(jLabel4)
                    .addComponent(textPrecoEstoque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(listCategoriaEst, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCategoriaEst))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bVoltarEst)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bVoltarEstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bVoltarEstActionPerformed
        TelaMenu tela = new TelaMenu();
        tela.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_bVoltarEstActionPerformed

    @SuppressWarnings("empty-statement")
    private void bAdicionarEstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAdicionarEstActionPerformed
        String newProduto = textProdutoEstoque.getText();
        String newPreco = textPrecoEstoque.getText();
        String newCategoria = listCategoriaEst.getSelectedItem().toString();
 
        try {
            EstoqueController.adicionaEstoque(newCategoria, newProduto, newPreco);
            JOptionPane.showMessageDialog(rootPane, newProduto+" cadastrado com Sucesso");
            this.setVisible(false);
            this.setVisible(true);
            
            jList1.setListData(ManipularArquivo.pegaDoArquivo("Sobremesa"));
            jList2.setListData(ManipularArquivo.pegaDoArquivo("Bebida"));
            jList3.setListData(ManipularArquivo.pegaDoArquivo("Comida"));
            
        } catch (IOException ex) {
            Logger.getLogger(TelaEstoque.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       
        
       
        
    }//GEN-LAST:event_bAdicionarEstActionPerformed

    private void textPrecoEstoqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textPrecoEstoqueActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textPrecoEstoqueActionPerformed

    public static void main(String args[]){
        
        
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new TelaEstoque().setVisible(true);
                    
                    
                    jList1.setListData(ManipularArquivo.pegaDoArquivo("Sobremesa"));
                    jList2.setListData(ManipularArquivo.pegaDoArquivo("Bebida"));
                    jList3.setListData(ManipularArquivo.pegaDoArquivo("Comida"));
                } catch (IOException ex) {
                    Logger.getLogger(TelaEstoque.class.getName()).log(Level.SEVERE, null, ex);
                }
                    
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bAdicionarEst;
    private javax.swing.JButton bVoltarEst;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private static javax.swing.JList<String> jList1;
    private static javax.swing.JList<String> jList2;
    private static javax.swing.JList<String> jList3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel labelCategoriaEst;
    private javax.swing.JLabel labelProdutoEst;
    private javax.swing.JLabel labelRestauranteEst;
    private javax.swing.JComboBox<String> listCategoriaEst;
    private javax.swing.JTextField textPrecoEstoque;
    private javax.swing.JTextField textProdutoEstoque;
    // End of variables declaration//GEN-END:variables
}
