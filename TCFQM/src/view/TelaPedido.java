package view;

import controller.BebidaController;
import controller.ComidaController;
import controller.EstoqueController;
import controller.ManipularArquivo;
import controller.SobremesaController;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Bebida;
import model.Cliente;
import model.Comida;
import model.ItemPedido;
import model.Pagamento;
import model.Pedido;
import model.Sobremesa;

public class TelaPedido  extends javax.swing.JFrame {

    public TelaPedido() {
        initComponents();
        
        
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelRestaurantePed = new javax.swing.JLabel();
        labelClientePed = new javax.swing.JLabel();
        bSalvarPed = new javax.swing.JButton();
        bCancelarPed = new javax.swing.JButton();
        clienteText = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane4 = new javax.swing.JScrollPane();
        jListComida = new javax.swing.JList<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        jListBebida = new javax.swing.JList<>();
        jScrollPane6 = new javax.swing.JScrollPane();
        jListSobremesa = new javax.swing.JList<>();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jScrollPane7 = new javax.swing.JScrollPane();
        obsLabel = new javax.swing.JTextArea();
        jLabel10 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        listCategoriaPagamento = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelRestaurantePed.setText("Tô Com Fome Quero Mais");

        labelClientePed.setText("Cliente");

        bSalvarPed.setText("Concluir");
        bSalvarPed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bSalvarPedActionPerformed(evt);
            }
        });

        bCancelarPed.setText("Cancelar");
        bCancelarPed.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bCancelarPedActionPerformed(evt);
            }
        });

        try {
            jListComida.setModel(new javax.swing.AbstractListModel<String>() {
                String[] strings = ManipularArquivo.pegaDoArquivo("Comida");
                public int getSize() { return strings.length; }
                public String getElementAt(int i) { return strings[i]; }
            });
        } catch (IOException ex) {
            Logger.getLogger(TelaEstoque.class.getName()).log(Level.SEVERE, null, ex);
        }
        jScrollPane4.setViewportView(jListComida);

        try {
            jListBebida.setModel(new javax.swing.AbstractListModel<String>() {
                String[] strings = ManipularArquivo.pegaDoArquivo("Bebida");
                public int getSize() { return strings.length; }
                public String getElementAt(int i) { return strings[i]; }
            });
        } catch (IOException ex) {
            Logger.getLogger(TelaEstoque.class.getName()).log(Level.SEVERE, null, ex);
        }
        jScrollPane5.setViewportView(jListBebida);

        try {
            jListSobremesa.setModel(new javax.swing.AbstractListModel<String>() {
                String[] strings = ManipularArquivo.pegaDoArquivo("Sobremesa");
                public int getSize() { return strings.length; }
                public String getElementAt(int i) { return strings[i]; }
            });
        } catch (IOException ex) {
            Logger.getLogger(TelaEstoque.class.getName()).log(Level.SEVERE, null, ex);
        }
        jScrollPane6.setViewportView(jListSobremesa);

        jLabel4.setText("Sobremesa");

        jLabel5.setText("Bebida");

        jLabel6.setText("Comida");

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        obsLabel.setColumns(20);
        obsLabel.setRows(5);
        jScrollPane7.setViewportView(obsLabel);

        jLabel10.setText("Observação");

        listCategoriaPagamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Debito", "Credito", "Dinheiro" }));

        jLabel1.setText("Forma de Pagamento");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(118, 118, 118)
                .addComponent(jLabel4)
                .addGap(238, 238, 238)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(183, 183, 183))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 114, Short.MAX_VALUE)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(81, 81, 81)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(119, 119, 119))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(listCategoriaPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(84, 84, 84))))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator4)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator2)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelClientePed)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(clienteText, javax.swing.GroupLayout.PREFERRED_SIZE, 393, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(labelRestaurantePed))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(176, 176, 176)
                .addComponent(bSalvarPed, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bCancelarPed)
                .addGap(200, 200, 200))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelRestaurantePed)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelClientePed)
                    .addComponent(clienteText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(56, 56, 56)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(listCategoriaPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bSalvarPed)
                    .addComponent(bCancelarPed))
                .addGap(69, 69, 69))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bSalvarPedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bSalvarPedActionPerformed
        String nomeCliente = clienteText.getText();
        
        String sobremesa = jListSobremesa.getSelectedValue().toString();
        Sobremesa objSobremesa;
        objSobremesa = SobremesaController.criaSobremesa(sobremesa);
        
        String bebida = jListBebida.getSelectedValue().toString();
        Bebida objBebida;
        objBebida = BebidaController.criaBebida(bebida);
        
        String comida = jListComida.getSelectedValue().toString();
        System.out.println(comida);
        Comida objComida;
        objComida = ComidaController.criaComida(comida);
        
        String pagamento = listCategoriaPagamento.getSelectedItem().toString();
        
        int total = objSobremesa.getPreco()+objBebida.getPreco()+objComida.getPreco();
        
        try {
            EstoqueController.retiraPedido("Comida", comida);
            EstoqueController.retiraPedido("Bebida", bebida);
            EstoqueController.retiraPedido("Sobremesa", sobremesa);
        } catch (IOException ex) {
            Logger.getLogger(TelaPedido.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
        JOptionPane.showMessageDialog(rootPane, "Sr "+nomeCliente+" pediu: \n"+objSobremesa.getNome()+"\n"+objComida.getNome()+"\n"+objBebida.getNome()+"\nObs: "+obsLabel.getText()+"\nForma de Pagamento: "+pagamento+"\n\nTOTAL: R$"+total+",00");
        
        this.setVisible(false);
        new TelaPedido().setVisible(true);
    }//GEN-LAST:event_bSalvarPedActionPerformed

    private void bCancelarPedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bCancelarPedActionPerformed
        TelaMenu tela = new TelaMenu();
        tela.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_bCancelarPedActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                String[] opcoes = {"bebida","comida","sobremesa"};
                
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bCancelarPed;
    private javax.swing.JButton bSalvarPed;
    private javax.swing.JTextField clienteText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JList<String> jListBebida;
    private javax.swing.JList<String> jListComida;
    private javax.swing.JList<String> jListSobremesa;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel labelClientePed;
    private javax.swing.JLabel labelRestaurantePed;
    private javax.swing.JComboBox<String> listCategoriaPagamento;
    private javax.swing.JTextArea obsLabel;
    // End of variables declaration//GEN-END:variables
}
