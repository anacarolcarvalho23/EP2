package controller;

import java.io.IOException;
import java.util.List;
import model.Sobremesa;

public class EstoqueController {

    public static void adicionaEstoque(String categoria, String produto,String preco) throws IOException{
        ManipularArquivo.gravaCliente(categoria, produto, preco);
        String[] estoque = ManipularArquivo.pegaDoArquivo(categoria);
//        for (int i=0; i< estoque.length;i++){
//            System.out.println(estoque[i]);
//        }
    }
    
    public static void retiraPedido(String categoria,String produto) throws IOException{
        String newProduto = produto;
        String[] estoque = ManipularArquivo.pegaDoArquivo(categoria);
        
        for (int i=0;i<estoque.length;i++){
            if(newProduto.equals(estoque[i])){
                for(i=i;i<=estoque.length-1;i++)
                {
                    if(i==estoque.length-1){
                        estoque[i]="";
                    }
                    else{
                        String swap = estoque[i+1];
                        estoque[i] = swap;
                    }
                }
                
            }
        }
        ManipularArquivo.escreveArquivo(categoria, estoque);
        
        
    }
    
    public static void imprimeLista(String[] lista){
        for(int i = 0;i<lista.length;i++){
            System.out.println(lista[i]);
        }
    }
}

