/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Bebida;

/**
 *
 * @author levimoraes
 */
public class BebidaController {
    public static Bebida criaBebida(String texto){
        Bebida bebidaObjeto = new Bebida();
        
        int total=0;
        String[] newTexto = texto.split(" "); 
        bebidaObjeto.setNome(newTexto[0]);
        bebidaObjeto.setPreco(Integer.valueOf(newTexto[1]));
       
        return bebidaObjeto;
    }
}
