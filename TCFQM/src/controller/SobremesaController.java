/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Sobremesa;

/**
 *
 * @author levimoraes
 */
public class SobremesaController {
    public static Sobremesa criaSobremesa(String texto){
        Sobremesa sobremesaObjeto = new Sobremesa();
        
        int total=0;
        String[] newTexto = texto.split(" "); 
        sobremesaObjeto.setNome(newTexto[0]);
        sobremesaObjeto.setPreco(Integer.valueOf(newTexto[1]));
       
        return sobremesaObjeto;
    }
}
