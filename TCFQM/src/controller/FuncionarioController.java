package controller;

import model.Funcionario;

public class FuncionarioController {
    
    public String retornaLogin(){
        Funcionario funcionario = new Funcionario();
        //System.out.println(funcionario.getLogin());
        return funcionario.getLogin();
    }
    
    public String retornaSenha(){
        Funcionario funcionario = new Funcionario();
        //System.out.println(funcionario.getSenha());
        return funcionario.getSenha();
    }
    
    public static boolean checaLoginSenha(String login, String senha){
        FuncionarioController funcControl = new FuncionarioController();
        
        String xLog = funcControl.retornaLogin();
        String xSen = funcControl.retornaSenha();
        
       // System.out.println(xLog + xSen);
       // System.out.println(login + senha);
        
        if ((xLog.equals(login)) && (xSen.equals(senha))){
            return true;
        }
        
        else {
            return false;
        }
    }
    
    
}
