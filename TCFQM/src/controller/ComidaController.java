/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Comida;

/**
 *
 * @author levimoraes
 */
public class ComidaController {
    public static Comida criaComida(String texto){
        Comida comidaObjeto = new Comida();
        
        int total=0;
        String[] newTexto = texto.split(" "); 
        comidaObjeto.setNome(newTexto[0]);
        comidaObjeto.setPreco(Integer.valueOf(newTexto[1]));
       
        return comidaObjeto;
    }
}
