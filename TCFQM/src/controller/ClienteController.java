/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author levimoraes
 */
public class ClienteController {
    public static void gravaCliente(String nome,String endereco, String telefone) throws IOException {
        // String auxiliar para inserir conteúdo no arquivo. Já é inicializada com o nome do cliente.
        //String texto = cliente.getNome();
        // Arquivo a ser criado.
        File file = new File("Clientes.txt");
        // Inserção no arquivo.
        BufferedWriter output = null;
        // Tente:
        try {
            // Buffer para gravar a saida.
            output = new BufferedWriter(new FileWriter(file, true));
                        
            output.write(nome+","+endereco+","+telefone);
            // Quando pular a linha começa um novo cliente, assim é possível organizar os dados.
            output.write(";\n");
            
        // Se não conseguir pegue o erro e mostre no console.
        } catch ( IOException e ) {
            // Nada a fazer.
        } finally {
            if ( output != null ) {
                output.close();
            }
        }
    }

    // Método de pegar conteúdo do arquivo.

//ALTERREI ESSE METODO, AGORA ELE DEVOLVE UM ARRAY COM OS PRODUTOS QUE ESTAO NO TXT
    public static String[] pegaCliente(String arquivo) throws IOException {
        // Cria instancia de classe pra ler arquivo.
        String frase = "";
        FileInputStream banco;
        // Nome do banco.
        banco = new FileInputStream(arquivo+".txt");
        // O metodo de pegar valor retorna um inteiro, então "aux" guarda esse retorno.
        int aux;
        char letra;
        try {
            // Enquanto tem coisa no arquivo...
                while((aux = banco.read()) != -1) {
                    // pega a letra do arquivo em forma de inteiro e convert pra char.
                    letra = (char)aux;
                    // imprime no console a letra.
                    frase+=letra;
                    //System.out.print(letra);				
                }
                
        } catch (IOException e) {
            // Nada a fazer.
        } finally {
            if (banco != null) {
                banco.close();
            }
        }
        //AQUI ELE COLOCA NO ARRAY E SEPARA A CADA ;
        String[] Produtos = frase.split(";");
        
        //RETORNA O ARRAY
        return Produtos;
    }
}
