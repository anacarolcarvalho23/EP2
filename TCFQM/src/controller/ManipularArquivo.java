package controller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.io.*;
import java.io.BufferedWriter;
import java.util.Scanner;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import model.Cliente;

public class ManipularArquivo {

public static void gravaCliente( String arquivo ,String texto,String preco) throws IOException {
        // String auxiliar para inserir conteúdo no arquivo. Já é inicializada com o nome do cliente.
        //String texto = cliente.getNome();
        // Arquivo a ser criado.
        File file = new File(arquivo+".txt");
        // Inserção no arquivo.
        BufferedWriter output = null;
        // Tente:
        try {
            // Buffer para gravar a saida.
            output = new BufferedWriter(new FileWriter(file, true));

            output.write(texto+" "+preco);

            output.write("\n");
            
        // Se não conseguir pegue o erro e mostre no console.
        } catch ( IOException e ) {
            // Nada a fazer.
        } finally {
            if ( output != null ) {
                output.close();
            }
        }
    }

    // Método de pegar conteúdo do arquivo.

//ALTERREI ESSE METODO, AGORA ELE DEVOLVE UM ARRAY COM OS PRODUTOS QUE ESTAO NO TXT
    public static String[] pegaDoArquivo(String arquivo) throws IOException {
        // Cria instancia de classe pra ler arquivo.
        String frase = "";
        FileInputStream banco;
        // Nome do banco.
        banco = new FileInputStream(arquivo+".txt");
        // O metodo de pegar valor retorna um inteiro, então "aux" guarda esse retorno.
        int aux;
        char letra;
        try {
            // Enquanto tem coisa no arquivo...
                while((aux = banco.read()) != -1) {
                    // pega a letra do arquivo em forma de inteiro e convert pra char.
                    letra = (char)aux;
                    // imprime no console a letra.
                    frase+=letra;
                    //System.out.print(letra);				
                }
                
        } catch (IOException e) {
            // Nada a fazer.
        } finally {
            if (banco != null) {
                banco.close();
            }
        }
        //AQUI ELE COLOCA NO ARRAY E SEPARA A CADA ;
        String[] Produtos = frase.split("\n");
        
        //RETORNA O ARRAY
        return Produtos;
    }
    
    public static void escreveArquivo(String arquivo,String[] produtos) throws FileNotFoundException, IOException{
         
        
        ManipularArquivo.deletaArquivo(arquivo);
        
        File file = new File(arquivo+".txt");
        BufferedWriter output = null;
        output = new BufferedWriter(new FileWriter(file, true));
        for(int i=0; i<produtos.length;i++){
        output.write(produtos[i]);
        output.write("\n");
        }
        output.close();
    
    }
    
    public static void deletaArquivo(String nome){
        String newNome = nome+".txt";
        File arquivo = new File(newNome);
        arquivo.delete();
    }
    
}


