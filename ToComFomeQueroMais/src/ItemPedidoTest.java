import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemPedidoTest extends TestCase{

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetQuantidade() {
		ItemPedido pedido = new ItemPedido();
		pedido.setQuantidade(10);
		assertEquals(10, pedido.getQuantidade());
	}

	@Test
	public void testSetQuantidade() {
		ItemPedido pedido = new ItemPedido();
		assertTrue(pedido.setQuantidade(10));
	}

	@Test
	public void testGetValor() {
		ItemPedido pedido = new ItemPedido();
		pedido.setValor(10.0);
		assertEquals(10.0, pedido.getValor());
	}

	@Test
	public void testSetValor() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetObservacao() {
		ItemPedido pedido = new ItemPedido();
		pedido.setObservacao("observacao");
		assertEquals("observacao", pedido.getObservacao());
	}

	@Test
	public void testSetObservacao() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetProduto() {
		ItemPedido pedido = new ItemPedido();
		Produto comida = new Produto();
		pedido.setProduto(comida);
		assertEquals(comida, pedido.getProduto());
	}

	@Test
	public void testSetProduto() {
		fail("Not yet implemented");
	}

}
