public class ItemPedido {

	private	int quantidade;
	private double valor;
	private String observacao;
	private Produto produto;
	
	public int getQuantidade() {
		return quantidade;
	}
	public boolean setQuantidade(int quantidade) {
		this.quantidade = quantidade;
		return true;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	
	
}
