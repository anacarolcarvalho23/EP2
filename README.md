# EP2 - OO (UnB - Gama)

1. Para abrir o arquivo no NetBeans, siga a seguinte instrução:
Os arquivos estarão na pasta TCFQM, sendo assim, já tem na pasta principal um arquivo .zip com o mesmo nome para ser importado na IDE.

2. Caso queira o arquivo executavel, ele se encontra no seguinte caminho:
EP2/TCFQM/dist 
Dentro dessa pasta se encontra o arquivo .jar do programa

3. O programa pode ser executado pela IDE, através do comando "shift+f6", caso execute atraves dele, comece executando a TelaLogin. 

#O usuário é "joao"
#A senha "12345"

4. É necessário acrescentar os produtos na tela do estoque primeiramente. 
Após cadastrar os produtos, vá para a tela do Pedido.
Os produtos cadastrados devem ser valor na forma de numero inteiro, sem número quebrado. 

5. Não é necessário que cadastre o cliente, entretanto, quando fizer o pedido, é necessário que se coloque o nome mesmo que não tenha sido previamente cadastrado, sendo esse um "cadastro simples", já que no restaurante não é necessário informar endereço e nem telefone, a opção do cadastro é para um possível sistema de entrega que tenha a necessidade de atribuir essas informações, futuramente.

6. Para efetuar o pedido, é necessário que o funcionario selecione cada um dos tres produtos, casa algum deles não seja pedido pelo cliente, uma sugestão é que seja cadastrado no estoque um produto "Sem escolha" com valor "0" para não ser acrescentado valor algum ao valor final da conta.

